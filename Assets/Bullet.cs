﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : Photon.MonoBehaviour
{
    public bool MoveDir = false; //false (right), true (left)

    public float MoveSpeed;

    public float DestroyTime;

    public float BulletDamage;

    private SpriteRenderer spriteRenderer; 
    
    private float verticalVelocity = 0f; 
    
    private float gravity = 9.8f; 
    
    private bool isStuck = false;
    
    private Rigidbody2D rb2d;

    private void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        rb2d = GetComponent<Rigidbody2D>();
        StartCoroutine("DestroyByTime");
    }

    IEnumerator DestroyByTime()
    {
        yield return new WaitForSeconds(DestroyTime);
        this.GetComponent<PhotonView>().RPC("DestroyObject", PhotonTargets.AllBuffered);
    }

    [PunRPC]
    public void ChangeDir_left()
    {
        MoveDir = true;
    }


    [PunRPC]
    public void DestroyObject()
    {
        Destroy(this.gameObject);
    }

    private void Update()
    {
        Debug.Log(isStuck);
        if (isStuck)
        {
            rb2d.velocity = Vector2.zero;
            rb2d.gravityScale = 0f; 
            return;
        }

        verticalVelocity -= gravity * Time.deltaTime;

        float horizontalMovement = MoveDir ? -MoveSpeed : MoveSpeed;
        rb2d.velocity = new Vector2(horizontalMovement, verticalVelocity);

        float angle = Mathf.Atan2(verticalVelocity, horizontalMovement) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(0, 0, angle);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {

        if (collision.gameObject.layer == 8)
        {
            Debug.Log("Ground collision detected");
            isStuck = true;
        }

        if (!photonView.isMine)
            return;

        PhotonView target = collision.gameObject.GetComponent<PhotonView>();
        if (target != null && (!target.isMine || target.isSceneView))
        {
            if (target.tag == "Player")
            {
                target.RPC("ReduceHealth", PhotonTargets.AllBuffered, BulletDamage);
            }

            this.GetComponent<PhotonView>().RPC("DestroyObject", PhotonTargets.AllBuffered);
        }
    }

}
