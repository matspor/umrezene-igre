﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Hashtable = ExitGames.Client.Photon.Hashtable;


public class MenuController : MonoBehaviour
{
    [SerializeField] private string VersioName = "0.1";
    [SerializeField] private GameObject UsernameMenu;
    [SerializeField] private GameObject ConnectPanel;

    [SerializeField] private InputField UsernameInput;
    [SerializeField] private InputField CreateGameInput;
    [SerializeField] private InputField JoinGameInput;

    [SerializeField] private GameObject StartButton;

    [SerializeField] private Image AvatarImage;

    private List<Color> AvailableAvatarColors = new List<Color>{
            new Color(0.9411764705882353f, 0.32941176470588235f, 0.32941176470588235f, 1f),
            Color.green,
            Color.blue,
            Color.yellow,
        };

    private int CurrentColorIndex = 0;

    private void Awake()
    {
        PhotonNetwork.ConnectUsingSettings(VersioName);
    }

    private void Start()
    {
        UsernameMenu.SetActive(true);
        SetUserColor(AvailableAvatarColors[0]);
    }

    private void OnConnectedToMaster()
    {
        PhotonNetwork.JoinLobby(TypedLobby.Default);
        Debug.Log("Connected");
    }

    public void ChangeUserNameInput()
    {
        if(UsernameInput.text.Length >= 3)
        {
            StartButton.SetActive(true);
        }
        else
        {
            StartButton.SetActive(false);
        }
    }

    public void SetUserName()
    {
        UsernameMenu.SetActive(false);
        PhotonNetwork.playerName = UsernameInput.text;
    }

    public void SetUserColor(Color color)
    {
        UpdateNetworkColor(color);
        UpdateUIAvatarColor(color);
    }

    public void UpdateNetworkColor(Color color)
    {
        Hashtable hash = new Hashtable();
        SerializableColor serializableColor = color;
        hash.Add("color", serializableColor.ToDictionary());
        PhotonNetwork.SetPlayerCustomProperties(hash);
    }

    public void UpdateUIAvatarColor(Color color)
    {
        Texture2D avatarTexture = LoadAvatarTexture();
        ChangeAvatarColor(avatarTexture, color);
        AvatarImage.sprite = Sprite.Create(avatarTexture, new Rect(0, 0, avatarTexture.width, avatarTexture.height), Vector2.one * 0.5f);
    }

    public void OnAvatarTap()
    {
        CurrentColorIndex++;
        CurrentColorIndex = CurrentColorIndex % AvailableAvatarColors.Count;
        SetUserColor(AvailableAvatarColors[CurrentColorIndex]);
    }

    Texture2D LoadAvatarTexture()
    {
        byte[] fileData = System.IO.File.ReadAllBytes("Assets/Sprites/Avatar.png");
        Texture2D texture = new Texture2D(2, 2);
        texture.LoadImage(fileData);
        texture.filterMode = FilterMode.Point;
        return texture;
    }

    void ChangeAvatarColor(Texture2D texture, Color color)
    {
        Color[] pixels = texture.GetPixels();

        for (int i = 0; i < pixels.Length; i++)
        {
            if (pixels[i] == AvailableAvatarColors[0])
            {
                // Convert red to color
                pixels[i] = color;
            }
        }

        texture.SetPixels(pixels);
        texture.Apply();
    }

    public void CreateGame()
    {
        PhotonNetwork.CreateRoom(CreateGameInput.text, new RoomOptions() { maxPlayers = 5 }, null);
    }

    public void JoinGame()
    {
        RoomOptions roomOptions = new RoomOptions();
        roomOptions.maxPlayers = 5;
        PhotonNetwork.JoinOrCreateRoom(JoinGameInput.text, roomOptions, TypedLobby.Default);
    }

    private void OnJoinedRoom()
    {
        PhotonNetwork.LoadLevel("MainGame");
    }

}
