﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : Photon.MonoBehaviour
{
    public PhotonView photonView;
    public Rigidbody2D rb;
    public Animator anim;
    public GameObject PlayerCamera;
    public SpriteRenderer sr;
    public Text PlayerNameText;
    public float MoveSpeed;

    public float jumpSpeed;
    public float moveInput;
    private bool isOnGround;
    public Transform playerPos;
    public float positionRadius;
    public LayerMask ground;
    private float airTimeCount;
    public float airTime;
    private bool inAir;

    public GameObject BulletObject;
    public Transform FirePos;

    public bool DisableInput = false;


    private void Awake()
    {
        if (photonView.isMine)
        {
            PlayerCamera.SetActive(true);
            PlayerNameText.text = PhotonNetwork.playerName;
        }
        else
        {
            PlayerNameText.text = photonView.owner.name;
            PlayerNameText.color = Color.cyan;
        }


        //TODO: change sprite color to GetPlayerColor() color
        Debug.Log("Player color:" + GetPlayerColor().ToString() + "for player name " + photonView.owner.name);
    }

    private Color GetPlayerColor()
    {
        Dictionary<string, float> colorDictionary = (Dictionary<string, float>)photonView.owner.CustomProperties["color"];

        Color receivedColor = new Color(
           colorDictionary["r"],
           colorDictionary["g"],
           colorDictionary["b"],
           colorDictionary["a"]
       );

        return receivedColor;
    }

    private void Update()
    {
        if (photonView.isMine && !DisableInput)
        {
            CheckInput();
        }
    }

    private void CheckInput()
    {
        isOnGround = Physics2D.OverlapCircle(playerPos.position, positionRadius, ground);
        if (isOnGround == true && Input.GetKeyDown(KeyCode.Space))
        {
            inAir = true;
            airTimeCount = airTime;
            rb.velocity = Vector2.up * jumpSpeed;
        }

        if (Input.GetKey(KeyCode.Space) && inAir == true)
        {
            if (airTimeCount > 0)
            {
                rb.velocity = Vector2.up * jumpSpeed;
                airTimeCount -= Time.deltaTime;
            }
            else
            {
                inAir = false;
            }
        }

        if (Input.GetKeyUp(KeyCode.Space))
        {
            inAir = false;
        }

        var move = new Vector3(Input.GetAxisRaw("Horizontal"), 0);
        transform.position += move * MoveSpeed * Time.deltaTime;

        if (Input.GetKeyDown(KeyCode.Mouse1))
        {
            Shoot();
        }


        if (Input.GetKeyDown(KeyCode.A))
        {
            photonView.RPC("FlipTrue", PhotonTargets.AllBuffered);
        }

        if (Input.GetKeyDown(KeyCode.D))
        {
            photonView.RPC("FlipFalse", PhotonTargets.AllBuffered);
        }


        if(Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.D))
        {
            anim.SetBool("isRunning", true);
        }
        else
        {
            anim.SetBool("isRunning", false);
        }

    }



private void Shoot()
{
    float rightOffset = 0.8f; 
    float leftOffset = 2.0f;

    if (!sr.flipX)
    {
        Vector2 spawnPosition = new Vector2(FirePos.transform.position.x + rightOffset, FirePos.transform.position.y);
        GameObject obj = PhotonNetwork.Instantiate(BulletObject.name, spawnPosition, Quaternion.identity, 0);
    }
    else
    {
        Vector2 spawnPosition = new Vector2(FirePos.transform.position.x - leftOffset, FirePos.transform.position.y);
        GameObject obj = PhotonNetwork.Instantiate(BulletObject.name, spawnPosition, Quaternion.identity, 0);
        obj.GetComponent<PhotonView>().RPC("ChangeDir_left", PhotonTargets.AllBuffered);
    }

    anim.SetTrigger("shootTrigger");
}



    [PunRPC]
    private void FlipTrue()
    {
        sr.flipX = true;
    }

    [PunRPC]
    private void FlipFalse()
    {
        sr.flipX = false;
    }

    [PunRPC]
    private void Teleport(Vector3 newPosition)
    {
        transform.position = newPosition;
    }

}
