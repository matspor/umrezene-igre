﻿using System;
using UnityEngine;
using System.Collections.Generic;

[System.Serializable]
public class SerializableColor
{

	public float r, g, b, a;

	public Color Color
	{
		get { return new Color(r, g, b, a); }
		set {
			r = value.r;
			g = value.g;
			b = value.b;
			a = value.a;
		}
	}

	//makes this class usable as Color, Color normalColor = mySerializableColor;
	public static implicit operator Color(SerializableColor instance)
	{
		return instance.Color;
	}

	//makes this class assignable by Color, SerializableColor myColor = Color.white;
	public static implicit operator SerializableColor(Color color)
	{
		return new SerializableColor { Color = color };
	}

	public Dictionary<string, float> ToDictionary()
    {
        return new Dictionary<string, float>
        {
            { "r", r },
            { "g", g },
            { "b", b },
            { "a", a }
        };
    }
}